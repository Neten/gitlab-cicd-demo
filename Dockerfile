FROM python:3.9-slim-buster 
RUN mkdir /app
WORKDIR /app
COPY requirements.txt /app
RUN pip install -r requirements.txt
ADD . /app
EXPOSE 7000
CMD ["gunicorn", "-b", "0.0.0.0:7000", "wsgi:app"]
